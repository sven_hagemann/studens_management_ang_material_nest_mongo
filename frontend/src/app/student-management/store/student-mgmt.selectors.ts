import { createSelector, createFeatureSelector, ActionReducerMap } from '@ngrx/store';
import { navBarReducer, NavBarState } from './reducers/side-nav.reducer';
import { studentReducer, StudentState } from './reducers/student.reducer';

export interface AppState {
  navBar: NavBarState;
  student: StudentState;
}

export const featureStateName = 'studentMgmt';

export const studentMgmtReducers: ActionReducerMap<AppState> = {
  navBar: navBarReducer, // Methode, welche ein Objekt vom Typ NavBarState zurück gibt.
  student: studentReducer, // Hier ein Objekt vom Typ StudentState
};

export const studentsFeatureState = createFeatureSelector<AppState>(featureStateName);

export const getNavbarVisibility = createSelector(studentsFeatureState, (state: AppState) => state.navBar.isVisible);

export const getNavbarMode = createSelector(studentsFeatureState, (state: AppState) => state.navBar.mode);
