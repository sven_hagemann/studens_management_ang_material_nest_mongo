import { createAction, props } from '@ngrx/store';

// pettierrc-ignore
export const setSideBarVisibility = createAction('[Home Page] Set Sidebar Visibility', props<{ value: boolean }>());

export const setSideBarMode = createAction('[Home Page] Set Sidebar Mode', props<{ value: string }>());
