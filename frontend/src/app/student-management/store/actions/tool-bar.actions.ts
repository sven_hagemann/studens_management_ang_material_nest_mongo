import { createAction } from '@ngrx/store';

export const toggleSideBar = createAction('[ToolBar] Toggle Sidebar');
