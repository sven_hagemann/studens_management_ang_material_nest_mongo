import { Action, createReducer, on } from '@ngrx/store';
// import * as ActionsTypes from '../actions/student.actions';
import { Student } from '../../shared/models/student';

export interface StudentListState {
  students: Student[];
  selectedStudent: Student;
  loading: boolean;
}

export const initialStudentListState: StudentListState = {
  students: [],
  selectedStudent: null,
  loading: false,
};

const reducerInternal = createReducer(
  initialStudentListState,
  //   on(ActionsTypes.setNavBarVisibility, (state, { value }) => ({
  //     ...state,
  //     navBar: {...state.navBar, isVisible: value },
  //   })),
  //   on(ActionsTypes.toggleNavBar, (state) => ({
  //     ...state,
  //     navBar: { ...state.navBar, isVisible: !state.navBar.isVisible },
  //   })),
);

export function studentReducer(state: StudentListState | undefined, action: Action) {
  return reducerInternal(state, action);
}
