import { Action, createReducer, on } from '@ngrx/store';
// import * as ActionsTypes from '../actions/student.actions';

export interface StudentState {
  id: string;
  name: string;
  email: string;
  section: string;
  subjects: Array<string>;
  gender: string;
  dob: Date;
}

export const initialStudentState: StudentState = {
  id: '',
  name: '',
  email: '',
  section: '',
  subjects: null,
  gender: '',
  dob: null,
};

const reducerInternal = createReducer(
  initialStudentState,
  //   on(ActionsTypes.setNavBarVisibility, (state, { value }) => ({
  //     ...state,
  //     navBar: {...state.navBar, isVisible: value },
  //   })),
  //   on(ActionsTypes.toggleNavBar, (state) => ({
  //     ...state,
  //     navBar: { ...state.navBar, isVisible: !state.navBar.isVisible },
  //   })),
);

export function studentReducer(state: StudentState | undefined, action: Action) {
  return reducerInternal(state, action);
}
