import { Action, createReducer, on } from '@ngrx/store';
import * as ActionsTypes from '../actions';

export interface NavBarState {
  isVisible: boolean;
  mode: string;
}

export const initialNavBarState: NavBarState = {
  isVisible: window.innerWidth < 768 ? false : true,
  mode: window.innerWidth < 768 ? 'over' : 'side',
};

const reducerInternal = createReducer(
  initialNavBarState,
  on(ActionsTypes.setSideBarVisibility, (state, { value }) => ({
    ...state,
    isVisible: value,
  })),
  on(ActionsTypes.toggleSideBar, (state) => ({
    ...state,
    isVisible: !state.isVisible,
  })),
  on(ActionsTypes.setSideBarMode, (state, { value }) => ({
    ...state,
    mode: value,
  })),
);

export function navBarReducer(state: NavBarState | undefined, action: Action) {
  return reducerInternal(state, action);
}
