import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddStudentComponent } from './components/home/side-nav/add-student/add-student.component';
import { EditStudentComponent } from './components/home/side-nav/edit-student/edit-student.component';
import { StudentListComponent } from './components/home/side-nav/student-list/student-list.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from '../auth/auth.guard';

const studentsRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'student-list',
        component: StudentListComponent,
      },
      {
        path: 'add-student',
        component: AddStudentComponent,
      },
      {
        path: 'edit-student/:id',
        component: EditStudentComponent,
      },
      {
        path: '',
        component: StudentListComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(studentsRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
