import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';

import { AddStudentComponent } from './components/home/side-nav/add-student/add-student.component';
import { EditStudentComponent } from './components/home/side-nav/edit-student/edit-student.component';
import { StudentListComponent } from './components/home/side-nav/student-list/student-list.component';
import { ApiService } from './shared/api.service';
import { AngularMaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './components/home/home.component';
import { AppRoutingModule } from './app.routing.module';
import { studentMgmtReducers, featureStateName } from './store/student-mgmt.selectors';
import { ToolbarComponent } from './components/home/toolbar/toolbar.component';
import { SideNavComponent } from './components/home/side-nav/side-nav.component';

// prettier-ignore
@NgModule({
  declarations: [
    AddStudentComponent,
    EditStudentComponent,
    StudentListComponent,
    HomeComponent,
    ToolbarComponent,
    SideNavComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    AppRoutingModule,
    StoreModule.forFeature(featureStateName, studentMgmtReducers)
  ],
  exports: [
    AngularMaterialModule
  ],
  providers: [ApiService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
