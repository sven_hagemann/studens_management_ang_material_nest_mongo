import { Component, OnInit } from '@angular/core';
import { AppState } from '../../../store/student-mgmt.selectors';
import { toggleSideBar } from '../../../store/actions';

import { Store } from '@ngrx/store';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  constructor(private readonly store: Store<AppState>) {}

  ngOnInit(): void {}

  toggle() {
    this.store.dispatch(toggleSideBar());
  }
}
