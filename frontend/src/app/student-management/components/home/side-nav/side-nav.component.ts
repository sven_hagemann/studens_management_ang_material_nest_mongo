import { Component, ViewChild } from '@angular/core';
import { getNavbarVisibility, getNavbarMode, AppState } from '../../../store/student-mgmt.selectors';

import { select, Store } from '@ngrx/store';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss'],
})
export class SideNavComponent {
  navBarVisibilityState$ = this.store.pipe(select(getNavbarVisibility));
  mode$ = this.store.pipe(select(getNavbarMode));

  @ViewChild('sidenav') sidenav: MatSidenav;

  constructor(private readonly store: Store<AppState>) {}
}
