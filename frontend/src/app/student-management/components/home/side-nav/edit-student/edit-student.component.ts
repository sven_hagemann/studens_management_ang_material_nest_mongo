import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';

import { ApiService } from '../../../../shared/api.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.scss'],
})
export class EditStudentComponent implements OnInit {
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  @ViewChild('chipList') chipList;
  @ViewChild('resetStudentForm') myNgForm;

  // @ViewChild(MatDatepicker) picker: MatDatepicker<Moment>;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  studentForm: FormGroup;
  subjectArray: string[] = [];
  SectioinArray: any = ['A', 'B', 'C', 'D', 'E'];

  ngOnInit() {
    this.initializeStudentForm();
  }

  constructor(
    public fb: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private actRoute: ActivatedRoute,
    private studentApi: ApiService,
  ) {
    // this.actRoute.paramMap.pipe(
    //   switchMap((params: ParamMap) =>
    //     this.studentApi.GetStudent(params.get('id')))
    // ).subscribe(data => {
    //   this.subjectArray = data.subjects;
    //   this.studentForm = this.fb.group({
    //     student_name: [data.student_name, [Validators.required]],
    //     student_email: [data.student_email, [Validators.required]],
    //     section: [data.section, [Validators.required]],
    //     subjects: [data.subjects],
    //     dob: [data.dob, [Validators.required]],
    //     gender: [data.gender],
    //   });
    // });

    const id = this.actRoute.snapshot.paramMap.get('id');
    this.studentApi.GetStudent(id).subscribe(data => {
      this.subjectArray = data.subjects;
      this.studentForm = this.fb.group({
        student_name: [data.student_name, [Validators.required]],
        student_email: [data.student_email, [Validators.required]],
        section: [data.section, [Validators.required]],
        subjects: [data.subjects],
        dob: [data.dob, [Validators.required]],
        gender: [data.gender],
      });
    });
  }

  /* Reactive student form */
  initializeStudentForm() {
    this.studentForm = this.fb.group({
      student_name: ['', [Validators.required]],
      student_email: ['', [Validators.required]],
      section: ['', [Validators.required]],
      subjects: [this.subjectArray],
      dob: ['', [Validators.required]],
      gender: ['male'],
    });
  }

  /* Add subject */
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    console.log('Value: ', value);

    // Add language
    if ((value || '').trim() && this.subjectArray.length < 5) {
      this.subjectArray.push(value.trim());
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  /* Remove subject */
  remove(subject: string): void {
    const index = this.subjectArray.indexOf(subject);
    if (index >= 0) {
      this.subjectArray.splice(index, 1);
    }
  }

  /* Get errors */
  public handleError = (controlName: string, errorName: string) => {
    return this.studentForm.controls[controlName].hasError(errorName);
  };

  /* Update student */
  updateStudentForm() {
    console.log('updateStudentForm: ', this.studentForm.get('dob').value);
    const id = this.actRoute.snapshot.paramMap.get('id');
    if (window.confirm('Are you sure you want to update?')) {
      this.studentApi.UpdateStudent(id, this.studentForm.value).subscribe(res => {
        this.ngZone.run(() => this.router.navigateByUrl('/student-management-home'));
        console.log('updateStudent');
      });
    }
  }
}
