import { HostListener, Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromActions from '../../store/actions';
import * as fromSelectors from '../../store/student-mgmt.selectors';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  navBarMode: string;

  constructor(private readonly store: Store<fromSelectors.AppState>) {
    this.store.pipe(select(fromSelectors.getNavbarMode)).subscribe((val) => (this.navBarMode = val));
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth < 768) {
      if (this.navBarMode === 'side') {
        this.store.dispatch(fromActions.setSideBarVisibility({ value: false }));
        this.store.dispatch(fromActions.setSideBarMode({ value: 'over' }));
      }
      // this.sidenav.fixedTopGap = 55
    } else if (event.target.innerWidth >= 768) {
      if (this.navBarMode === 'over') {
        this.store.dispatch(fromActions.setSideBarVisibility({ value: true }));
        this.store.dispatch(fromActions.setSideBarMode({ value: 'side' }));
      }
      // this.sidenav.fixedTopGap = 55;
    }
  }
}
