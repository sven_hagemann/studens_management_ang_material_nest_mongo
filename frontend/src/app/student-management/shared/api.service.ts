import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Student } from './models/student';
import { Observable, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  endpoint = 'http://localhost:3000/students';
  header = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) {}

  GetAllStudents(): Observable<any> {
    console.log('All Students');

    return this.http.get(this.endpoint);
  }

  GetStudent(id): Observable<any> {
    return this.http.get(`${this.endpoint}/${id}`, { headers: this.header }).pipe(
      map((res: Response) => {
        console.log('XXX');
        return res || {};
      }),
      catchError(this.errorMgmt),
    );
  }

  AddStudent(data: Student): Observable<any> {
    return this.http.post(this.endpoint, data).pipe(catchError(this.errorMgmt));
  }

  UpdateStudent(id, data: Student): Observable<any> {
    return this.http.patch(`${this.endpoint}/${id}`, data, { headers: this.header }).pipe(catchError(this.errorMgmt));
  }

  DeleteStudent(id): Observable<any> {
    return this.http.delete(`${this.endpoint}/${id}`).pipe(catchError(this.errorMgmt));
  }

  // Error handling
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
