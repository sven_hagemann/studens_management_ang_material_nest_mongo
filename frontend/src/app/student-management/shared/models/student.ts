export class Student {
  id: string;
  name: string;
  email: string;
  section: string;
  subjects: Array<string>;
  gender: string;
  dob: Date;
}
