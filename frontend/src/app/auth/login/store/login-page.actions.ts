import { createAction, props } from '@ngrx/store';

// pettierrc-ignore
export const login = createAction(
    '[Login Page] Login',
    props<{ username: string; password: string }>()
);

// Todo: https://ngrx.io/guide/store/actions
