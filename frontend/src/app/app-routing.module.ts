import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './components/home-page/home-page.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: 'app-home-page', component: HomePageComponent },
  { path: 'student-management-home',
    loadChildren: () => import('./student-management/app.module')
      .then(m => m.AppModule),
      canLoad: [AuthGuard]
  },
  { path: '', pathMatch: 'full', redirectTo: '/app-home-page' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      // { enableTracing: true }, // <-- debugging purposes only
    ),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
