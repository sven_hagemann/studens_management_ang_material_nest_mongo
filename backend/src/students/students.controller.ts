// prettier-ignore
import { Controller, Post, Body, Get, Param, Patch, Delete } from '@nestjs/common';
import { StudentsService } from './students.service';

@Controller('students')
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}

  @Get()
  // @Header('Content-Typ', 'text/html') => Set explicit header properties (import { Header } from '@nestjs/common')
  async getAllStudents() {
    const students = await this.studentsService.getAllStudents();
    return students;
  }

  @Get(':id')
  async getStudent(@Param('id') studentId: string) {
    const student = await this.studentsService.getSingleStudent(studentId);
    return student;
  }

  @Post()
  async addStudent(
    @Body('student_name') studentName: string,
    @Body('student_email') studentMail: string,
    @Body('section') studentSection: string,
    @Body('subjects') studentSubjets: Array<string>,
    @Body('gender') studentGender: string,
    @Body('dob') studentDate: Date,
  ) {
    // service will return a Promise therefor we have to wait or handle with .then()
    // prettier-ignore
    const generatedId = await this.studentsService.insertStudent( studentName, studentMail, studentSection, studentSubjets, studentGender, studentDate );
    return { id: generatedId };
  }

  @Patch(':id')
  async updateStudent(
    @Param('id') id: string,
    @Body('student_name') name: string,
    @Body('student_email') email: string,
    @Body('section') section: string,
    @Body('subjects') subjects: Array<string>,
    @Body('gender') gender: string,
    @Body('dob') dob: Date,
  ) {
    // prettier-ignore
    await this.studentsService.updateStudent(id, name, email, section, subjects, gender, dob );
    return null;
  }

  @Delete(':id')
  async deleteStudent(@Param('id') id: string) {
    await this.studentsService.deleteStudent(id);
    return null;
  }
}
