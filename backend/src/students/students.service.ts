import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Student } from './student.model';

@Injectable()
export class StudentsService {
    // private students: Student[] = [];

    // @InjectModel('Student') => related to students.module.ts where we defined the model name
    // Model<Student> => extend the type from the interface "Student(student.model.ts)" by more functionality 
    constructor(@InjectModel('Student') private readonly studentModel: Model<Student>){}

    async getAllStudents(){
        const students = await this.studentModel.find().exec();
        // returns a new object to format the output
        return students.map(student => ({
            id: student.id,
            student_name: student.student_name,
            student_email: student.student_email,
            section: student.section,
            subjects: student.subjects,
            gender: student.gender,
            dob: student.dob
        })) as Student[]; // type of the interface "Student"
    }

    async getSingleStudent(id: string){
        const student = await this.findStudent(id);
        return {
            id: student.id,
            student_name: student.student_name,
            student_email: student.student_email,
            section: student.section,
            subjects: student.subjects,
            gender: student.gender,
            dob: student.dob
        };
    }

    async insertStudent(name: string, email: string, section: string, subjects: Array<String>, gender: String, dob: Date){
        const newStudent = new this.studentModel({ // instance of the model with mongoose feature e.g. Save()
            student_name: name,
            student_email: email,
            section, // shorted because parameter name and model property name is same
            subjects,
            gender,
            dob
        });

        // result is a Promise therefore wait here
        // but we can use "...save().then(...)" as well
        const result = await newStudent.save(); // create a full mongoDB query and save date to DB - by mongoose
        return result.id as string;        
    }

    async updateStudent(id: string, name: string, email: string, section: string, subjects: Array<string>, gender: string, dob: Date){
        const updatedStudent = await this.findStudent(id);

        if (name) {
            updatedStudent.student_name = name;
        }
        
        if (email) {
            updatedStudent.student_email = email;
        }
        if (section) {
            updatedStudent.section = section;
        }
        if (subjects) {
            updatedStudent.subjects = subjects;
        }
        if (gender) {
            updatedStudent.gender = gender;
        }
        if (dob) {
            updatedStudent.dob = dob;
        }

        updatedStudent.save();
    }

    async deleteStudent(id: string){
        const result = await this.studentModel.deleteOne({_id: id}).exec();

        if (result.n === 0) {
            throw new NotFoundException('Could not find student.');
        }
    }

    private async findStudent(id: string): Promise<Student>{
        // this.studentModel.findOne({ student_name: 'Sven Hagemann' }, function (err, name) {
        //     console.log(name);
        // });

        // more features: https://mongoosejs.com/docs/api/model.html

        let student;
        try {
            student = await this.studentModel.findById(id);            
        } catch (error) {
            throw new NotFoundException('Could not find student.');
        }

        if (!student) {
            throw new NotFoundException('Could not find student.');
        }

        return student;
    }
}
