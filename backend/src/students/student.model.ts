import * as mongoose from 'mongoose';

// Javascript
export const StudentSchema = new mongoose.Schema({
  student_name: { type: String, required: true },
  student_email: String,
  section: String,
  subjects: Array,
  gender: String,
  dob: Date,
});

// prettier-ignore
// Typescript
export interface Student extends mongoose.Document {  // npm i --save @types/mongoose
    id: string;
    student_name: string;
    student_email: string;
    section: string;
    subjects: Array<string>;
    gender: string;
    dob: Date;
}
